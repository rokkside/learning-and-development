# This project is for all matters related to Learning & Development.

## Handbook Pages Related to [Learning & Development](https://about.gitlab.com/handbook/people-group/learning-and-development/) 

- [Career Development](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/)
- [Certifications](https://about.gitlab.com/handbook/people-group/learning-and-development/certifications/)
- [Coaching](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/coaching/)
- [Emotional Intelligence](https://about.gitlab.com/handbook/people-group/learning-and-development/emotional-intelligence/)
- [Guidance on Feedback](https://about.gitlab.com/handbook/people-group/guidance-on-feedback/)
- [Leadership Forum](https://about.gitlab.com/handbook/people-group/learning-and-development/leadership-forum/)
- [Leadership Toolkit](https://about.gitlab.com/handbook/people-group/leadership-toolkit/##)
- [Manager Challenge](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge/)
- [Psychological Safety](https://about.gitlab.com/handbook/people-group/learning-and-development/emotional-intelligence/psychological-safety/)
- [Social Styles](https://about.gitlab.com/handbook/people-group/learning-and-development/emotional-intelligence/social-styles/)
- [Transitioning to a Manager Role](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-development/)
- [Underperformance](https://about.gitlab.com/handbook/underperformance/)

## Issue Templates

* [Becoming a Manager Issue](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md)
* [New L&D Team Member](https://gitlab.com/gitlab-com/people-group/learning-and-development/-/blob/master/.gitlab/issue_templates/new-learning-and-development-team-member.md)

## Training Items

This list is for all training related items at GitLab. Training has been organized by functional groups and been linked to their location in the handbook. 

GitLab encourages functional groups to develop functional specific training for personal growth and professional development. When creating training, please notify the Learning & Development Team at `learning@gitlab.com` so we can add it to our training list. This page serves as the central locaitons for all trainings at GitLab.

### GitLab General 

*  [GitLab Training tracks](https://about.gitlab.com/training/)
*  [GitLab University](https://docs.gitlab.com/ee/university/)
*  [Learn @ GitLab](https://about.gitlab.com/learn/)
*  [Pathfactory flows - example path](https://learn.gitlab.com/c/a-beginner-s-guide-t?x=GVkN_U&lb_referrer=https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)

### Legal

*  N/A

### Finance

*  N/A

### People

*  Learning Sessions
   *  [Live Learning](https://about.gitlab.com/handbook/people-group/learning-and-development/#live-learning) Sessions 
      *  [Ally Training](https://about.gitlab.com/company/culture/inclusion/ally-training/)
      *  [Inclusion Training](https://about.gitlab.com/company/culture/inclusion/inclusion-training/)
      *  [Receiving Feedback](https://about.gitlab.com/handbook/people-group/guidance-on-feedback/#responding-to-feedback)
      *  [Communicating Effectively & Responsibly Through Text](https://about.gitlab.com/company/culture/all-remote/effective-communication/)
      *  [Compensation Review: Manager Cycle (Compaas)](https://www.youtube.com/watch?v=crkPeOjkqTQ&feature=youtu.be) - just a youtube video
   * [Action Learning](https://about.gitlab.com/handbook/people-group/learning-and-development/#action-learning) 
   * [Leadership Forum](https://about.gitlab.com/handbook/people-group/learning-and-development/leadership-forum/)  
*  [Certifications & Knowledge Assessments](https://about.gitlab.com/handbook/people-group/learning-and-development/certifications/)
   *  [Values](https://about.gitlab.com/handbook/values/#gitlab-values-certification)
   *  [Communication](https://about.gitlab.com/handbook/communication/#communication-certification)
   *  [Ally](https://about.gitlab.com/company/culture/inclusion/ally-training/#ally-certification)
   *  [Inclusion](https://about.gitlab.com/company/culture/inclusion/inclusion-training/#inclusion-certification)
   *  [GitLab 101](https://about.gitlab.com/handbook/people-group/learning-and-development/certifications/gitlab-101/)
*  [Career Development](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/)
*  [Anti-Harassment](https://about.gitlab.com/handbook/people-group/learning-and-development/#common-ground-harassment-prevention-training) 
*  [New Manager Enablement](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-development/)
* [People Specialist team and People Experience team training issue](https://gitlab.com/gitlab-com/people-group/people-operations-and-experience-team-training/-/blob/master/.gitlab/issue_templates/new-hire-training.md)
* [People Specialist team - Contractor Conversion Starter Kit](https://gitlab.com/gitlab-com/people-group/people-operations-and-experience-team-training/-/blob/master/.gitlab/issue_templates/new-hire-training.md)
* [Onboarding](https://about.gitlab.com/handbook/general-onboarding/)/Training Issues
   *  [Onboarding Issue](https://gitlab.com/gitlab-com/people-group/employment-templates-2/-/blob/master/.gitlab/issue_templates/onboarding.md)
   *  [Interview Training](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/interview_training.md)
   *  [Becoming a GitLab Manager](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md)
   *  [New L&D Team Members](https://gitlab.com/gitlab-com/people-group/learning-and-development/-/blob/master/.gitlab/issue_templates/new-learning-and-development-team-member.md)
* [Leadership Toolkit](https://about.gitlab.com/handbook/people-group/leadership-toolkit/)

### Engineering

*  [Developer Onboarding](https://about.gitlab.com/handbook/developer-onboarding/) 
*  [Engineering Quality Team Onboarding](https://about.gitlab.com/handbook/engineering/quality/onboarding/ )
*  [Support Onboarding](https://about.gitlab.com/handbook/support/onboarding/)
*  [Rails Performance Workshop](https://drive.google.com/drive/folders/1kXMilTokysTZ0GtnRrS3Ru132AYe0-_g )
*  [Calendar Blocking Workshop](https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/441)
*  [UX Research Training](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/)
*  [UX Designers Guide to Contributing to UI Changes](https://about.gitlab.com/handbook/engineering/ux/ux-resources/designers-guide-to-contributing-ui-changes-in-gitlab/)

### Product

*  [Product Onboarding](https://gitlab.com/gitlab-com/Product/-/blob/master/.gitlab/issue_templates/PM-onboarding.md)
   *  [Iteration Training](https://gitlab.com/gitlab-com/Product/-/blob/master/.gitlab/issue_templates/iteration-training.md)

### Marketing

*  [Marketing Onboarding](https://gitlab.com/gitlab-com/marketing/onboarding/blob/master/.gitlab/issue_templates/non_xdr_function_onboarding.md)

### Sales

*  [Sales Training Handbook](https://about.gitlab.com/handbook/sales/training/)
*  [Sales Quick Start](https://about.gitlab.com/handbook/sales/onboarding/sales-learning-path/)
*  [Sales Enablement Sessions](https://about.gitlab.com/handbook/sales/training/sales-enablement-sessions/)
*  [Sales Kick Off (SKO)](https://about.gitlab.com/handbook/sales/training/SKO/)
*  [Customer Success Skills Exchange](https://about.gitlab.com/handbook/sales/training/customer-success-skills-exchange/)
*  [Customer Success GitLab Demos platform & catalog](https://gitlabdemo.com/)

### Channel

*  [Reseller onboarding](https://about.gitlab.com/handbook/resellers/onboarding/)

### Professional Services

*  [Framework](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/framework/)
*  [SKUs](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/SKUs/)
*  [Customer Services Guided Explorations](https://gitlab.com/guided-explorations)

