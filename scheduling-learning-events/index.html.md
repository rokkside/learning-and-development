## Scheduling Learning and Development Events

### In the `#peopleops` slack channel, send the following message: 

Hi People Ops Specialists - I need help scheduling an event on the GitLab Team Meetings Calendar. I will include the details in this thread! 


### In the thread of your message, include the following: 

Dates (Prefer all 3 events scheduled on the same day if possible): [insert date options here] 

Length: x minutes 

Times: We will need 3 events total between the dates above with similar times to previous L&D sessions —> so ~7:30am PT, ~2:00pm PT, & ~6:30pm PT 

Required attendees/alt hosts: [add names here]

Who should be invited: [ex. All GitLab Team members] 

Recording/Live Streamed: Private recording

**To Include in Calendar Invite**

Title of Event: [Add title here]

Info to include in body of event 

Target Audience: [ex. All GitLab team members] 

Agenda: [add link]

Details: [add in a brief description of the event]

Event Info: This event will be a x minute Live Learning style event. If this is a 50 minute session, there will be a breakout group part of this session to work thorugh scenarios. See more details on Live Learning sessions here: 
https://about.gitlab.com/handbook/people-group/learning-and-development/#live-learning 

Questions before the event?: Reach out to the Learning & Developemnt team in Slack (#learninganddevelopment) or email `learning@gitlab.com`.
