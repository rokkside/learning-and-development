## Live Learning

<!--
## README FIRST

Title this issue with `Live Learning: [Learning Topic] - [Date of Session]`.
Assign this issue to L&D team members who will be involved.
-->


**Live Learning Session Info**

{insert info about session here}


**Scheduled Sessions**
* {date + time} 
* {date + time}
* {date + time}


**Key People** 

Host: {add GitLab handle for L&D Team Member here}

Subject Matter Expert: {add GitLab handle for person who will presenting the content here}

People Business Partner (if necessary): {add GitLab handle here}


**Documents**

Slides: {add link here}

Agenda: {add link here}

Feedback Form: {add link here}


**Action Items**

Before completing a Learning session, please complete all of the following: 

1. [ ] Announce in slack 1 week out (#company-announcements & #learninganddevelopment)
1. [ ] Create an agenda document to be added to the calendar invite & link above
1. [ ] Create a slide deck with content to be discussed during the session & link above
1. [ ] Send out calendar invites on the GitLab Team Meetings calendar for Learning session at least 1 week out. Work with People Ops Specilaists/EBA's. Info to include: event title, group to invite (all gitlabbers, managers, etc), agenda doc link, any info for the body of the invite, potential/preferred times. 
1. [ ] Add information to the handbook (link MRs in issue comments)
1. [ ] Determine faciltator for session

After Learning session: 

1. [ ] Send out answers to any pending questions
1. [ ] Upload recording to YouTube Unfiltered so others can access
1. [ ] Embed video on appropriate handbook page 
1. [ ] Collect Feedback via Form
1. [ ] How many GitLab team members attended the live call: {insert number here}


**Learning Objectives** 
1. {insert list here}


**Target Audience**

*  ex. All GitLab team members 

**Facilitation Guide**

Use the suggested [Live Learning formats](https://about.gitlab.com/handbook/people-group/learning-and-development/#live-learning) as a reference for your facilitation guide. 

| Timing | Topic | Methodology | 
| ------ | ------ | ------ | 
| x mins | Introduction & Objectives | Slides | 
| x mins | xyz | xyz | 
| x mins | xyz | xyz | 
| x mins | xyz | xyz | 
| x mins | xyz | xyz | 

/label ~"Learning & Development"
