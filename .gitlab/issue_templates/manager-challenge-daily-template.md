<!--
## README FIRST

Title this issue with `Manager Challenge Program: Day [x].
Assign this issue to L&D team members.
-->

## Manager Challenge Program - Daily Challenges Template

More details to come.

## Day [x] - [Topic]

## Challenge Overview: 

## Action Items:

### Relevant Links

[add]()  

[links]() 

[here]() 

### Additional Notes: 

Once you have completed the tasks for today, add a comment to this issue. If you have questions about this section, please add them as a comment to this issue. 

[Add screenshot of the daily slide]