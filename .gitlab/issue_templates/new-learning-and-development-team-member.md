<!--
## README FIRST

Title this issue with `New L&D Team Member: [First & Last Name].
Assign this issue to new L&D team members.
-->

## Learning & Development 

Welcome to the Learning & Development team at GitLab. We are so excited to have you! This onboarding issue outlines key items about Learning and Development at GitLab. Please complete this issue during your second week at GitLab. 

### What a Day Looks like 

**Handbook iterations**

- Upgrading L&D handbook pages e.g. adding videos / images / slide decks 

**Support smooth organisation of L&D activities**

- Organise and manage the L&D board
- Manage learning logistics and administrative duties (tracking of Knowledge Assessment and Certification completion, scheduling sessions, Zapier maintenance) 
- Scheduling and running the logistics for all Live Learning events 
- Supporting the adoption of the Learning Management System and Content library for GitLab team members

**Content creation**

- Finalise materials for GitLab 201 and 301 and Merge Rights training - 3 weeks/120 hours
- Contribute to the development Manager challenge materials, handbook page updates, and training content in coordination with L&D Partner and Specialist 
- Develop bite size training modules based off existing information in the handbook
- Support L&D team in the design and development of role based learning paths using Handbook material and off-the-shelf content
- Work with L&D Specialist to uplevel “Becoming a Manager” issue that incorporates Manager Challenge materials 
- Develop Knowledge Assessments for Manager competencies
- Create Diversity, Inclusion, and Belonging learning paths leveraging off-the-shelf content and Handbook material

### Skills Needed for Successes
- GitLab tech savviness and ability to use and teach others on how to use the GitLab tool
- Clear and concise communication with strong interpersonal skills
- Strong organizational skills
- Results orientated
- Learner-first mindset
- Manager of One
- Bias for action
- Collaboration
- Distilling complex information into digestible parts
- People centric

### Success Measures 
- Delivery of materials on time for the 4 week manager challenge 
- 1st iteration rolled out of 201, 301 and Merge Rights training
- Effective and timely management of KA’s, certifications etc.
- Optimising our Learning administration processes
- Launching DIB learning paths to the GitLab community 

----------------------------------------------------------------

## Tasks to Complete

### New Team Member Tasks
*  [ ] Review What a Day Looks Like section above 
*  [ ] Review Skills needed for Success section above
*  [ ] Review Success Measures section above 
*  [ ] Review [L&D page](https://about.gitlab.com/handbook/people-group/learning-and-development/) 
*  [ ] Review [L&D Roadmap](https://about.gitlab.com/handbook/people-group/learning-and-development/#gitlab-learning-strategy--roadmap)
*  [ ] Review [L&D issue board](https://gitlab.com/gitlab-com/people-group/learning-and-development/-/boards/1281903)
* [ ] Read the [comprehensive guide to Learning and Development](https://www.digitalhrtech.com/learning-and-development/) by Digital HR Tech
* [ ] Read the Harvard Business Review article on [Where Companies Go Wrong with Learning and Development](https://hbr.org/2019/10/where-companies-go-wrong-with-learning-and-development)
* [ ] Read the McKinsey & Company's article on the [essential components of successful L&D strategy](https://www.mckinsey.com/business-functions/organization/our-insights/the-essential-components-of-a-successful-l-and-d-strategy)
* [ ] Checkout and bookmark the [Chief Learning Officer](https://www.chieflearningofficer.com/) website. It is an industry leading site for learning and development!

### Existing L&D Team Member Tasks
*  [ ] Add to `learning@gitlab.com` alias (access request)
*  [ ] Access to Zapier login info 
*  [ ] Add to L&D Folder in Google Drive 
*  [ ] Access to WILL Learning 

/label ~"Learning & Development"
